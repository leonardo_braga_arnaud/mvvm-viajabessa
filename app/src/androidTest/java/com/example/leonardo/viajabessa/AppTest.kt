package com.example.leonardo.viajabessa


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.test.runner.AndroidJUnit4
import android.test.UiThreadTest
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages
import com.example.leonardo.viajabessa.vm.TravelListViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class AppTest {

    var travelListViewModel: TravelListViewModel = TravelListViewModel()

    private lateinit var travelPackageTest: TravelPackages.TravelPackage

    @Mock
    lateinit var observer: Observer<List<TravelPackages.TravelPackage>>

    @Before
    fun setupTravelListViewModel() {
        MockitoAnnotations.initMocks(this)
        travelPackageTest = TestUtils.createTestTravelPackage()
        travelListViewModel.start()
    }

    @Test
    @UiThreadTest
    fun pageShouldResetWhenRefreshing() {

        travelListViewModel.refresh()

        assertEquals(1, travelListViewModel.getCurrentPage())
    }

    @Test
    @UiThreadTest
    fun shouldGetPackagesAfterUpdate() {

        travelListViewModel.travelPackagesObservable.observeForever(observer)

        (travelListViewModel.travelPackagesObservable as MutableLiveData).value = listOf(travelPackageTest)

        verify(observer).onChanged(listOf(travelPackageTest))
    }


}
