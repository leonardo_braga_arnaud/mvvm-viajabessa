package com.example.leonardo.viajabessa

import android.arch.lifecycle.Observer
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.example.leonardo.viajabessa.model.db.AppDatabase
import com.example.leonardo.viajabessa.model.db.dao.TravelPackageDao
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.io.IOException

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private var travelPackageDao: TravelPackageDao? = null
    private var db: AppDatabase? = null
    private val travelPackage = TestUtils.createTestTravelPackage()

    @Mock
    lateinit var observer: Observer<List<TravelPackages.TravelPackage>>

    @Before
    fun createDb() {
        MockitoAnnotations.initMocks(this)
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        travelPackageDao = db?.travelPackageDao()
    }

    @Test
    @Throws(Exception::class)
    fun writeTravelPackageAndReadInList() {
        val all = travelPackageDao?.getAll()
        all?.observeForever(observer)
        val list = listOf(travelPackage)
        travelPackageDao?.insertAll(list)
        verify(observer).onChanged(list)
        Thread.sleep(1000)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db?.close()
    }

}
