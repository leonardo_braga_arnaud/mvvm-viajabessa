package com.example.leonardo.viajabessa

import android.app.Application
import com.example.leonardo.viajabessa.di.AppComponent
import com.example.leonardo.viajabessa.di.AppModule
import com.example.leonardo.viajabessa.di.DaggerAppComponent

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class App : Application() {

    lateinit var appComponent: AppComponent

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()

    }

}