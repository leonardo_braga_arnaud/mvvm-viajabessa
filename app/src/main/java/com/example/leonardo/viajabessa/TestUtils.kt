package com.example.leonardo.viajabessa

import com.example.leonardo.viajabessa.model.db.entity.TravelPackages

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TestUtils {

    companion object {

        fun createTestTravelPackage(): TravelPackages.TravelPackage {
            return TravelPackages.TravelPackage(12345, "TesteLand",
                    123.45,
                    "http://google.com.br/teste.jpg",
                    "Descrição de Teste")
        }

    }
}