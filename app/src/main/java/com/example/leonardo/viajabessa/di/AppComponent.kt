package com.example.leonardo.viajabessa.di

import com.example.leonardo.viajabessa.model.TravelPackageRepository
import com.example.leonardo.viajabessa.vm.TravelListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(travelViewModel: TravelListViewModel)
    fun inject(travelPackageRepository: TravelPackageRepository)

}