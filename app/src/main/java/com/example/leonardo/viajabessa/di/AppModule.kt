package com.example.leonardo.viajabessa.di

import android.app.Application
import android.arch.persistence.room.Room
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.model.db.AppDatabase
import com.example.leonardo.viajabessa.model.remote.DeviceInfoInterceptor
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .connectTimeout(application.getString(R.string.connect_timeout).toLong(), TimeUnit.SECONDS)
                .writeTimeout(application.getString(R.string.write_timeout).toLong(), TimeUnit.SECONDS)
                .readTimeout(application.getString(R.string.timeout).toLong(), TimeUnit.SECONDS)
                .cache(Cache(application.cacheDir, application.getString(R.string.max_cache_size).toLong()))
                .addInterceptor(logging)
                .addInterceptor(DeviceInfoInterceptor())
                .build()

        return Retrofit.Builder()
                .baseUrl(application.getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    fun provideConnectivityObservable(): Observable<Connectivity> {
        return ReactiveNetwork.observeNetworkConnectivity(application.applicationContext)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    @Provides
    @Singleton
    fun provideAppDatabase(): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, application.getString(R.string.db_name))
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }

}