package com.example.leonardo.viajabessa.model

import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.model.db.AppDatabase
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages
import com.example.leonardo.viajabessa.model.remote.TravelPackagesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TravelPackageRepository : BaseRepository() {

    lateinit var retrofit: Retrofit
        @Inject set

    lateinit var db: AppDatabase
        @Inject set

    var allTravelPackagesCall: Call<TravelPackages>? = null

    var totalPages: Int = 0

    init {
        com.example.leonardo.viajabessa.App.instance.appComponent.inject(this)
    }

    fun loadPage(page: Int) {
        setLoading(true)
        allTravelPackagesCall?.cancel()
        allTravelPackagesCall = retrofit.create(TravelPackagesService::class.java).getTravelPackages(page)
        allTravelPackagesCall?.enqueue(object : Callback<TravelPackages> {
            override fun onResponse(call: Call<TravelPackages>?, response: Response<TravelPackages>?) {
                setLoading(false)
                if (response?.isSuccessful == true) {
                    val travelPackages = response.body()
                    if (travelPackages != null) {
                        totalPages = travelPackages.totalPages
                        AsyncTask.execute { db.travelPackageDao().insertAll(travelPackages.data) }
                        return
                    }
                }
                postMessage(R.string.error_sync_data)

            }

            override fun onFailure(call: Call<TravelPackages>?, t: Throwable?) {
                setLoading(false)
                postMessage(R.string.error_sync_data)

            }
        })
    }

    fun travelPackages(): LiveData<List<TravelPackages.TravelPackage>> {
        return db.travelPackageDao().getAll()
    }

    fun travelPackage(id: Long): List<TravelPackages.TravelPackage> {
        return db.travelPackageDao().getTravelPackage(id)
    }

}

