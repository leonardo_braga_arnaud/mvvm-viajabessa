package com.example.leonardo.viajabessa.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.leonardo.viajabessa.model.db.dao.TravelPackageDao
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
@Database(entities = [(TravelPackages.TravelPackage::class)], version = 10, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun travelPackageDao(): TravelPackageDao
}