package com.example.leonardo.viajabessa.model.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
@Dao
interface TravelPackageDao {

    @Query("SELECT * FROM travel_packages")
    fun getAll(): LiveData<List<TravelPackages.TravelPackage>>

    @Query("SELECT * FROM travel_packages WHERE id = :travelPackageId")
    fun getTravelPackage(travelPackageId: Long): List<TravelPackages.TravelPackage>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(travelPackage: List<TravelPackages.TravelPackage>)

    @Query("DELETE FROM travel_packages WHERE id = 99999")
    fun deleteTestEntry()


}