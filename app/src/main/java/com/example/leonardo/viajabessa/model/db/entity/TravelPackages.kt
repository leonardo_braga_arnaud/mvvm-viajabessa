package com.example.leonardo.viajabessa.model.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
data class TravelPackages(
        val message: String,
        val totalPages: Int,
        val data: ArrayList<TravelPackage>) {

    @Entity(tableName = "travel_packages")
    data class TravelPackage(

            @PrimaryKey
            @ColumnInfo(name = "id")
            val id: Long,

            @ColumnInfo(name = "title")
            val title: String,

            @ColumnInfo(name = "value")
            val value: Double,

            @ColumnInfo(name = "image")
            val image: String,

            @ColumnInfo(name = "description")
            val description: String) {

        val formattedValue: String
            get() {
                val formatter = NumberFormat.getCurrencyInstance(
                        Locale.getDefault()) as DecimalFormat
                val symbols = formatter.decimalFormatSymbols
                symbols.currencySymbol = "R$ "
                formatter.decimalFormatSymbols = symbols
                formatter.maximumFractionDigits = 2
                formatter.minimumFractionDigits = 2
                return formatter.format(value)
            }

    }

}