package com.example.leonardo.viajabessa.model.remote

import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class DeviceInfoInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()

        val request = original.newBuilder()
                .header("Manufacturer", Build.MANUFACTURER)
                .header("Model", Build.MODEL)
                .header("Version", Build.VERSION.SDK_INT.toString())
                .header("Version_Release", Build.VERSION.RELEASE)
                .method(original.method(), original.body())
                .build()

        return chain.proceed(request)
    }
}
