package com.example.leonardo.viajabessa.model.remote

import com.example.leonardo.viajabessa.model.db.entity.TravelPackages
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
interface TravelPackagesService {

    @GET("travel_packages/{page}")
    fun getTravelPackages(@Path("page") page: Int): Call<TravelPackages>
}