package com.example.leonardo.viajabessa.ui

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.leonardo.viajabessa.R

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
abstract class BaseActivity : AppCompatActivity() {

    abstract fun getProgressBar(): View

    fun showSnackMessage(messageResId: Int?) {
        val contentView = findViewById<View>(android.R.id.content)
        if (contentView == null || messageResId == null) return
        Snackbar.make(contentView, messageResId, Snackbar.LENGTH_LONG).show()
    }

    fun saveLong(key: String, value: Long, bundle: Bundle?) {
        bundle?.putLong(key, value)
        getSharedPreferences(getString(R.string.app_preferences_key), Context.MODE_PRIVATE)
                .edit()
                .putLong(key, value)
                .apply()
    }

    fun restoreLong(key: String, bundle: Bundle?): Long {
        return bundle?.getLong(key)
                ?: intent?.extras?.getLong(key)
                ?: getSharedPreferences(getString(R.string.app_preferences_key), Context.MODE_PRIVATE)
                        .getLong(key, 0)
    }

}