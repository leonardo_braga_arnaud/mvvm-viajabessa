package com.example.leonardo.viajabessa.ui

import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
abstract class BaseFragment : Fragment() {

    abstract fun getProgressBar(): View

    fun showSnackbar(messageResId: Int?) {
        if (view == null || messageResId == null) return
        Snackbar.make(view!!, messageResId, Snackbar.LENGTH_LONG).show()
    }

    fun loading(b: Boolean?) {
        getProgressBar().visibility = if (b == true) View.VISIBLE else View.GONE
    }

}