package com.example.leonardo.viajabessa.ui.travelPackages

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.leonardo.viajabessa.R
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

    }
}