package com.example.leonardo.viajabessa.ui.travelPackages

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.ui.BaseActivity
import com.example.leonardo.viajabessa.vm.TravelDetailViewModel
import kotlinx.android.synthetic.main.activity_travelpackage_detail.*

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TravelPackageDetailActivity : BaseActivity() {

    private lateinit var viewModel: TravelDetailViewModel
    private var travelPackageId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        travelPackageId = restoreLong(ITEM_ID, savedInstanceState)

        setContentView(R.layout.activity_travelpackage_detail)
        setupToolbar()
        setupListeners()
        setupViewModel()

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        saveLong(ITEM_ID, travelPackageId, outState)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getProgressBar(): View {
        return detailProgressBar
    }

    private fun setupToolbar() {
        detailToolbar.title = ""
        setSupportActionBar(detailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupListeners() {
        topBuyFab.setOnClickListener { viewModel.buy() }

        bottomBuyFab.setOnClickListener { viewModel.buy() }

        appBarLayout.addOnOffsetChangedListener({ _, verticalOffset ->
            var verticalDiff = Math.abs(verticalOffset) - appBarLayout.totalScrollRange
            val changeFabLimit = resources.getDimensionPixelSize(R.dimen.change_fab_limit)
            if (verticalDiff > changeFabLimit) verticalDiff = 0
            if (verticalDiff == 0) bottomBuyFab.show()
            else bottomBuyFab.hide()
        })
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders
                .of(this)
                .get(TravelDetailViewModel::class.java)
        viewModel.start()

        viewModel.travelPackageObservable.observe(this, Observer {
            supportActionBar?.title = it?.title
            descriptionTextView.text = it?.description
            priceTextView.text = it?.formattedValue
            Glide.with(this).load(it?.image).into(packageImageView)
        })

        viewModel.message.observe(this, Observer {
            showSnackMessage(it)
        })

        AsyncTask.execute { viewModel.travelPackageId.postValue(travelPackageId) }
    }

    companion object {

        const val ITEM_ID = "item_id"

        fun start(fragment: Fragment, itemId: Long) {
            val intent = Intent(fragment.context, TravelPackageDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putLong(ITEM_ID, itemId)
            intent.putExtras(bundle)
            fragment.startActivity(intent)
        }
    }
}
