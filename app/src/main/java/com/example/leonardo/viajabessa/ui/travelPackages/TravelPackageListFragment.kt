package com.example.leonardo.viajabessa.ui.travelPackages

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.ui.BaseFragment
import com.example.leonardo.viajabessa.vm.TravelListViewModel
import com.mugen.Mugen
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TravelPackageListFragment : BaseFragment(),
        TravelPackageListRecyclerViewAdapter.TravelPackageListener {

    private lateinit var viewModel: TravelListViewModel
    private lateinit var adapter: TravelPackageListRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(activity!!).get(TravelListViewModel::class.java)
        viewModel.start()

        adapter = TravelPackageListRecyclerViewAdapter(emptyList(), this)

        val columnCount = view.resources.getInteger(R.integer.list_column_count)
        travelsRecyclerView.layoutManager =
                if (columnCount == 1) LinearLayoutManager(view.context)
                else GridLayoutManager(view.context, columnCount)

        travelsRecyclerView.adapter = adapter

        travelsSwipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }

    }

    override fun getProgressBar(): View {
        return mainProgressBar
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.travelPackagesObservable.observe(this, Observer {
            if (it != null) {
                adapter.updateValues(it)
            }
        })

        viewModel.message.observe(this, Observer { error ->
            showSnackbar(error)
            travelsSwipeRefreshLayout.isRefreshing = false
        })

        viewModel.loading.observe(this, Observer { b ->
            val isLoading = b == true
            loading(isLoading)
            if (!isLoading) travelsSwipeRefreshLayout.isRefreshing = false
        })

        Mugen.with(travelsRecyclerView, viewModel.mugenCallbacks).start()

    }

    override fun onTravelPackageClick(id: Long) {
        TravelPackageDetailActivity.start(this@TravelPackageListFragment, id)
    }
}
