package com.example.leonardo.viajabessa.ui.travelPackages

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages
import kotlinx.android.synthetic.main.item_travelpackage.view.*

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TravelPackageListRecyclerViewAdapter(private var _values: List<TravelPackages.TravelPackage>,
                                           private val travelPackageListener: TravelPackageListener) :
        RecyclerView.Adapter<TravelPackageListRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_travelpackage, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item = _values[position]
        val iv = holder.itemView

        holder.item?.let {
            iv.titleTextView.text = it.title
            iv.priceTextView.text = it.formattedValue

            Glide.with(iv.context)
                    .load(it.image)
                    .into(iv.packageImageView)

            iv.setOnClickListener { _ ->
                travelPackageListener.onTravelPackageClick(it.id)
            }
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).clear(holder.itemView.packageImageView)
    }

    override fun getItemCount(): Int {
        return _values.size
    }

    fun updateValues(list: List<TravelPackages.TravelPackage>) {
        val lastPos = _values.size - 1
        _values = list
        if (list.size > _values.size) {
            notifyItemInserted(lastPos)
        } else {
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var item: TravelPackages.TravelPackage? = null
    }

    interface TravelPackageListener {
        fun onTravelPackageClick(id: Long)
    }
}
