package com.example.leonardo.viajabessa.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.example.leonardo.viajabessa.model.BaseRepository

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
abstract class BaseViewModel : ViewModel() {

    var loading: LiveData<Boolean> = MutableLiveData()
    var message: LiveData<Int> = MutableLiveData()

    abstract val repository: BaseRepository

    open fun start() {
        loading = Transformations.map(repository.loading, {
            return@map it
        })
        message = Transformations.map(repository.message, {
            return@map it
        })
    }

    fun postMessage(strId: Int) {
        (message as MutableLiveData<Int>).postValue(strId)
    }
}