package com.example.leonardo.viajabessa.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.model.TravelPackageRepository
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TravelDetailViewModel : BaseViewModel() {

    override val repository = TravelPackageRepository()

    val travelPackageId = MutableLiveData<Long>()
    val travelPackageObservable: LiveData<TravelPackages.TravelPackage> =
            Transformations.map(travelPackageId, { id ->
                return@map repository.travelPackage(id)[0]
            })

    fun buy() {
        postMessage(R.string.buy)
    }

    override fun start() {

    }


}