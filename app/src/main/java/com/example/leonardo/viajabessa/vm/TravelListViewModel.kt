package com.example.leonardo.viajabessa.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import com.example.leonardo.viajabessa.App
import com.example.leonardo.viajabessa.R
import com.example.leonardo.viajabessa.model.TravelPackageRepository
import com.example.leonardo.viajabessa.model.db.entity.TravelPackages
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity
import com.mugen.MugenCallbacks
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class TravelListViewModel : BaseViewModel() {

    override val repository = TravelPackageRepository()

    lateinit var connectivityObservable: Observable<Connectivity>
        @Inject set

    private val page = MutableLiveData<Int>()

    val travelPackagesObservable: LiveData<List<TravelPackages.TravelPackage>> =
            Transformations.map(repository.travelPackages(), {
                return@map it
            })

    val mugenCallbacks: MugenCallbacks = object : MugenCallbacks {

        override fun onLoadMore() {
            page.value = (page.value ?: 0) + 1
        }

        override fun isLoading(): Boolean {
            return (loading as MutableLiveData<Boolean>).value == true
        }

        override fun hasLoadedAllItems(): Boolean {
            return page.value ?: 1 >= repository.totalPages
        }
    }

    private val pageObserver = Observer<Int> {
        repository.loadPage(it ?: 1)
    }

    override fun start() {
        super.start()

        App.instance.appComponent.inject(this)
        observeConnectivity()

        page.observeForever(pageObserver)
    }

    private fun observeConnectivity() {
        connectivityObservable.subscribe({
            if (loading.value != true) {
                if (it.isAvailable) {
                    if (travelPackagesObservable.value == null) {
                        page.value = 1
                    }
                } else {
                    postMessage(R.string.working_offline)
                }
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        repository.allTravelPackagesCall?.cancel()
        page.removeObserver(pageObserver)
    }

    fun refresh() {
        page.value = 1
    }

    fun getCurrentPage(): Int {
        return page.value ?: 1
    }

}