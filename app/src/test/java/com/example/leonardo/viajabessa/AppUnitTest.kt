package com.example.leonardo.viajabessa

import com.example.leonardo.viajabessa.ui.travelPackages.TravelPackageListRecyclerViewAdapter
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
class AppUnitTest {

    @Test
    fun travelPackageAdapterShouldUpdateList() {
        val travelPackage = TestUtils.createTestTravelPackage()
        val listener = object : TravelPackageListRecyclerViewAdapter.TravelPackageListener {
            override fun onTravelPackageClick(id: Long) {}
        }
        val adapter = TravelPackageListRecyclerViewAdapter(listOf(travelPackage), listener)
        val updatedList = listOf(travelPackage, travelPackage)
        try {
            adapter.updateValues(updatedList)
        } catch (ignored: NullPointerException) {
        }
        assertEquals(2, adapter.itemCount)
    }

}
